Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: esptool
Source: https://github.com/espressif/esptool/releases
 The files listed in the Files-Excluded field have subsequently been removed
 from the source package for lack of copying permission statements or for
 otherwise not conforming to the DFSG.
Files-Excluded:
 test/*
 ecdsa/*

Files: *
Copyright: 
 2014-2016 Fredrik Ahlberg, Angus Gratton, Espressif Systems (Shanghai) PTE LTD
License: GPL-2+

Files: esptool.py
Comment:
 Binary stub code purged out of this file by debian/uupdate script due to DFSG.
Copyright: 
 2014-2016 Fredrik Ahlberg, Angus Gratton, Espressif Systems (Shanghai) PTE LTD
License: GPL-2+

Files: espefuse.py espsecure.py
Copyright: 2016 Espressif Systems (Shanghai) PTE LTD
License: GPL-2+

Files: debian/*
Copyright: 2017 Milan Kupcevic <milan@debian.org>
License: GPL-2+

Files:
 flasher_stub/include/slip.h
 flasher_stub/ld/*.ld
 flasher_stub/include/stub_flasher.h
 flasher_stub/wrap_stub.py
Copyright: 2016 Cesanta Software Limited
License: GPL-2+

Files:
 flasher_stub/stub_commands.c
 flasher_stub/include/stub_commands.h
 flasher_stub/include/soc_support.h
 flasher_stub/include/stub_write_flash.h
Copyright: 2016-2019 Espressif Systems (Shanghai) PTE LTD
License: GPL-2+

Files: flasher_stub/Makefile
Copyright: 2016 Cesanta Software Limited & Angus Gratton
License: GPL-2+

Files:
 flasher_stub/include/rom_functions.h
 flasher_stub/slip.c
 flasher_stub/stub_flasher.c
 flasher_stub/stub_write_flash.c
Copyright: 
 2016-2017 Cesanta Software Limited,
 2016-2019 Espressif Systems (Shanghai) PTE LTD
License: GPL-2+

License: GPL-2+
 # This program is free software; you can redistribute it and/or modify it under
 # the terms of the GNU General Public License as published by the Free Software
 # Foundation; either version 2 of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful, but WITHOUT
 # ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 # FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License along with
 # this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
 # Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian GNU systems, the complete text of the GNU General Public License 
 version 2 can be found in `/usr/share/common-licenses/GPL-2'.

Files:
 flasher_stub/miniz.c
 flasher_stub/include/miniz.h
Copyright: 2013 Rich Geldreich <richgel99@gmail.com>
License: Unilicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 .
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <http://unlicense.org/>

Files: pyaes/*
Copyright: 2014 Richard Moore
License: Expat
 # Permission is hereby granted, free of charge, to any person obtaining a copy
 # of this software and associated documentation files (the "Software"), to deal
 # in the Software without restriction, including without limitation the rights
 # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 # copies of the Software, and to permit persons to whom the Software is
 # furnished to do so, subject to the following conditions:
 #
 # The above copyright notice and this permission notice shall be included in
 # all copies or substantial portions of the Software.
 #
 # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 # LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 # THE SOFTWARE.


